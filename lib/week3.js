'use strict'

var fs = require('fs')
var utils = require('./utils')

exports.readGraphFromFile = function (fileName) {
  return fs.readFileSync(fileName, {encoding: 'utf8'})
    .split('\n')
    .filter(function (line) { return line.trim() !== '' })
    .reduce(function (graph, line) {
      var vertices = line.trim().split(/\s+/).map(Number)
      graph[vertices[0]] = vertices.slice(1)
      return graph
    }, {})
}

function vertices(graph) {
  return Object.keys(graph)
}

function cloneGraph(object) {
  return Object.getOwnPropertyNames(object).reduce(function (newObject, prop) {
    newObject[prop] = object[prop].slice()
    return newObject
  }, {})
}

function mergeVertices(graph, a, b) {
  if (a == null || b == null) return
  utils.removeElement(graph[a], b)
  utils.removeElement(graph[b], a)
  graph[b].forEach(function (vertex) {
    utils.replaceBy(graph[vertex], b, a)
  })
  Array.prototype.push.apply(graph[a], graph[b])
  delete graph[b]
}

function removeLoops(graph) {
  vertices(graph).forEach(function (vertex) {
    utils.removeElement(graph[vertex], Number(vertex))
  })
}

exports.contraction = function (graph) {
  graph = cloneGraph(graph)
  while (vertices(graph).length > 2) {
    var a = Number(utils.sample(vertices(graph)))
    var b = utils.sample(graph[a])
    mergeVertices(graph, a, b)
  }
  removeLoops(graph)
  return graph
}

function countPaths(graph) {
  var vs = vertices(graph)
  return vs.length < 2 ? 0 : graph[vs[0]].length
}

function iterations(graph) {
  var n = vertices(graph).length
  return Math.ceil(n * n * Math.log(n))
}

exports.minCut = function (graph) {
  var min = Infinity
  var counter = iterations(graph)
  while (counter--) {
    var paths = countPaths(exports.contraction(graph))
    if (min > paths) min = paths
  }
  return min
}

