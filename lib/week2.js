'use strict'

exports.quicksort = function (array, preparer) {
  if (typeof preparer !== 'function') preparer = exports.first
  return sort(array, 0, array.length, preparer)
}

function preparePivot(array, start, pivotIndex) {
  var tmp = array[start]
  array[start] = array[pivotIndex]
  array[pivotIndex] = tmp
}

// Question 1
exports.first = function (array, start, length) {
  preparePivot(array, start, start)
}

// Question 2
exports.final = function (array, start, length) {
  preparePivot(array, start, start + length - 1)
}

// Question 3
exports.median = function (array, start, length) {
  var middle = start + (Math.ceil(length / 2) - 1)
  var final = start + length - 1
  var pivotIndex
  var pivot = [array[start], array[middle], array[final]]
    .sort(function (a, b) {
      return a - b
    })[1]
  if (array[start] === pivot) pivotIndex = start
  else if (array[middle] === pivot) pivotIndex = middle
  else pivotIndex = final
  preparePivot(array, start, pivotIndex)
}

exports.partition = function (array, start, length, preparer) {
  preparer(array, start, length)
  var tmp
  var i = start + 1
  for (var j = start + 1; j < start + length; j++) {
    if (array[j] < array[start]) {
      tmp = array[j]
      array[j] = array[i]
      array[i] = tmp
      i++
    }
  }

  var pivotIndex = i - 1
  tmp = array[pivotIndex]
  array[pivotIndex] = array[start]
  array[start] = tmp
  return pivotIndex
}

function sort(array, start, length, preparer) {
  if (length > 1) {
    var pivotIndex = exports.partition(array, start, length, preparer)
    return (length - 1) +
      sort(array, start, pivotIndex - start, preparer) +
      sort(array, pivotIndex + 1, length - (pivotIndex - start + 1), preparer)
  } else {
    return 0
  }
}
