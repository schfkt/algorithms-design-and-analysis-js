'use strict'

var assert = require('assert')
var fs = require('fs')
var path = require('path')
var week2 = require('../index').week2

describe('Solutions for week 2 assignments', function () {
  describe('Checks correctness of pivot preparation procedure', function () {
    it('Prepares the first element as a pivot', function () {
      var array = [2, 6, 8, 2, 3, 4, 1, 9]
      var prepared = [2, 6, 8, 2, 3, 4, 1, 9]

      week2.first(array, 2, 6)

      assert.deepEqual(array, prepared)
    })

    it('Prepares the final element as a pivot', function () {
      var array = [9, 6, 5, 2, 3, 4, 1, 0, 7, 6]
      var prepared = [9, 6, 5, 2, 1, 4, 3, 0, 7, 6]

      week2.final(array, 4, 3)

      assert.deepEqual(array, prepared)
    })

    it('Prepares the median element as a pivot', function () {
      var array = [9, 4, 7, 1, 6, 7, 8, 5, 6, 2, 0, 10]
      var prepared = [9, 4, 6, 1, 7, 7, 8, 5, 6, 2, 0, 10]

      week2.median(array, 2, 6)

      assert.deepEqual(array, prepared)
    })
  })

  describe('Check correctness of the partition subroutine', function () {
    it('Correctly partitions an array using the first element as a pivot', function () {
      var array = [3, 2, 5, 1, 8, 4, 7, 6]
      var partitioned = [1, 2, 3, 5, 8, 4, 7, 6]

      var pivotIndex = week2.partition(array, 0, array.length, week2.first)

      assert.deepEqual(array, partitioned)
      assert.equal(pivotIndex, 2)
    })

    it('Correctly partitions an array using the final element as a pivot', function () {
      var array = [3, 2, 5, 1, 8, 4, 7, 6]
      var partitioned = [3, 2, 5, 1, 4, 6, 7, 8]

      var pivotIndex = week2.partition(array, 0, array.length, week2.final)

      assert.deepEqual(array, partitioned)
      assert.equal(pivotIndex, 5)
    })

    it('Correctly partitions an array using the median element as a pivot', function () {
      var array = [3, 2, 5, 1, 8, 4, 7, 6, 9]
      var partitioned = [3, 2, 5, 1, 8, 4, 7, 6, 9]

      var pivotIndex = week2.partition(array, 0, array.length, week2.final)

      assert.deepEqual(array, partitioned)
      assert.equal(pivotIndex, 8)
    })
  })

  describe('General tests for the quicksort algorithm', function () {
    it('Correctly sorts an array with one element', function () {
      var array = [42]
      var sorted = [42]

      week2.quicksort(array)

      assert.deepEqual(array, sorted)
    })

    it('Correctly sorts an array with even length', function () {
      var array = [3, 0, 9, 6, 5, 8, 7, 2, 4, 1]
      var sorted = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

      week2.quicksort(array)

      assert.deepEqual(array, sorted)
    })

    it('Correctly sorst and array with odd length', function () {
      var array = [-10, 100, 42, 69, 100500, -99, 66]
      var sorted = [-99, -10, 42, 66, 69, 100, 100500]

      week2.quicksort(array)

      assert.deepEqual(array, sorted)
    })

    it('Correctly sorts an array that has duplicates', function () {
      var array = [1, 1, 1, 3, 6, 6, 0, -10, 6, 0, 3, 6, -10]
      var sorted = [-10, -10, 0, 0, 1, 1, 1, 3, 3, 6, 6, 6, 6]

      week2.quicksort(array)

      assert.deepEqual(array, sorted)
    })

    it('Correctly sorts an array using final element as a pivot', function () {
      var array = [7, 8, 9, 6, 5, 1, 2, 4, 5, 8, 6]
      var sorted = [1, 2, 4, 5, 5, 6, 6, 7, 8, 8, 9]

      week2.quicksort(array, week2.final)

      assert.deepEqual(array, sorted)
    })

    it('Correctly sorts an array using median element as a pivot', function () {
      var array = [7, 8, 9, 6, 5, 1, 2, 4, 5, 8, 6]
      var sorted = [1, 2, 4, 5, 5, 6, 6, 7, 8, 8, 9]

      week2.quicksort(array, week2.median)

      assert.deepEqual(array, sorted)
    })

  })

  describe('Answers for the questions', function () {
    var array
    var opts = {encoding: 'utf8'}
    var file = path.join(__dirname, './fixtures/QuickSort.txt')

    beforeEach(function () {
      array = fs.readFileSync(file, opts)
        .split('\n')
        .map(Number)
        .slice(0, 10000)
    })

    it('Checks answer for the first question', function () {
      var result = week2.quicksort(array)

      assert.equal(result, 162085)
    })

    it('Checks answer for the second question', function () {
      var result = week2.quicksort(array, week2.final)

      assert.equal(result, 164123)
    })

    it('Checks answer for the third question', function () {
      var result = week2.quicksort(array, week2.median)

      assert.equal(result, 138382)
    })
  })
})
