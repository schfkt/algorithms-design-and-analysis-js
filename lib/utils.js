'use strict'

exports.random = function (min, max) {
  return min + Math.floor(Math.random() * (max - min + 1))
}

exports.sample = function (array) {
  return array[exports.random(0, Math.max(0, array.length - 1))]
}

// TODO: implement using replaceBy?
exports.removeElement = function (array, element) {
  var index
  while ((index = array.indexOf(element)) !== -1) {
    array.splice(index, 1)
  }
}

exports.replaceBy = function (array, element, replacement) {
  var index
  while ((index = array.indexOf(element)) !== -1) {
    array.splice(index, 1, replacement)
  }
}

