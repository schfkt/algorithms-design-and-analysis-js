'use strict'

var assert = require('assert')
var utils = require('../lib/utils')

describe('utils::removeElement', function () {
  it('Removes specified element from an array', function () {
    var original = [1, 2, 3, 4, 5, 5, 3, 5, 7, 5]
    var modified = [1, 2, 3, 4, 3, 7]

    utils.removeElement(original, 5)

    assert.deepEqual(original, modified)
  })
})

describe('utils::replaceBy', function () {
  it('Replaces specified element in an array', function () {
    var original = [4, 1, 3, 5, 6, 7, 4, 9, 4]
    var modified = [8, 1, 3, 5, 6, 7, 8, 9, 8]

    utils.replaceBy(original, 4, 8)

    assert.deepEqual(original, modified)
  })
})

