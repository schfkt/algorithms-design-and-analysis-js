'use strict'

var assert = require('assert')
var path = require('path')
var week3 = require('../lib/week3')

describe('Reading a graph from a file', function () {
  it('Successfully reads a graph for a question', function () {
    var fileName = path.join(__dirname, './fixtures/kargerMinCut.txt')

    var graph = week3.readGraphFromFile(fileName)
    var adjacentVertices = [
      165, 48, 10, 141, 13, 43, 3, 173, 183, 104, 177, 72, 36, 77, 188, 90,
      136, 51, 163, 70, 130, 105, 61
    ]

    assert.equal(Object.keys(graph).length, 200)
    assert.deepEqual(graph[190], adjacentVertices)
  })
})

describe('Contraction routine', function () {
  it('Correctly reduces a graph to the one that has 2 vertices', function () {
    var graph = {
      1: [2, 3],
      2: [1, 3, 4],
      3: [1, 2, 4],
      4: [2, 3]
    }

    var result = week3.contraction(graph)

    Object.keys(result).forEach(function (vertex) {
      assert(result[vertex].length >= 2 && result[vertex].length <= 3)
    })
  })

  it('Removes loops from a graph', function () {
    var graph = {
      1: [1, 1, 2, 2],
      2: [1, 1, 2, 2, 2]
    }

    var result = week3.contraction(graph)

    assert.deepEqual(result[1], [2, 2])
    assert.deepEqual(result[2], [1, 1])
  })

  it('Does not modifies the original graph', function () {
    var graph = {
      1: [2, 3],
      2: [1, 3, 4],
      3: [1, 2, 4],
      4: [2, 3]
    }

    week3.contraction(graph)

    assert.deepEqual(graph[1], [2, 3])
    assert.deepEqual(graph[2], [1, 3, 4])
    assert.deepEqual(graph[3], [1, 2, 4])
    assert.deepEqual(graph[4], [2, 3])
  })
})

describe('Find the MinCut using Kerger algorithm', function () {
  it('Correctly find the MinCut for a small graph', function () {
    var graph = {
      1: [2, 3],
      2: [1, 3, 4],
      3: [1, 2, 4],
      4: [2, 3]
    }

    var minCut = week3.minCut(graph)

    assert.equal(minCut, 2)
  })

  // NOTE: it takes a lot of time for this graph (~210k iterations)
  // it('Correctly find the MinCut for the assignment', function () {
  //   var fileName = path.join(__dirname, './fixtures/kargerMinCut.txt')
  //   var graph = week3.readGraphFromFile(fileName)

  //   var minCut = week3.minCut(graph)

  //   assert.equal(minCut, 17)
  // })
})

